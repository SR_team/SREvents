#include "SREvents.h"

#if __has_include( <windows.h>)
#	if __has_include( <llmo/SRHook.hpp>)
#		include <windows.h>
#	endif
#else
static_assert( false, "Can't include <windows.h>. This library support only win32 projects!" );
#endif
#if __has_include( <llmo/ccallhook.h>)
#	include <llmo/ccallhook.h>
#endif

SREvents::SREvents() {
	eventHook = false;

#if __has_include( <llmo/SRHook.hpp>)
	events.onBefore += std::tuple{ this, &SREvents::WndProc };
	events.install( 0, 0, false );
#endif

#if __has_include( <llmo/ccallhook.h>)
	//script = new CCallHook( reinterpret_cast<void *>( 0x0046A1E7 ), 5, 0, cp_after, sc_all, e_jmp );
	main = new CCallHook( reinterpret_cast<void *>( 0x00748DA3 ), 6, 0, cp_after, sc_all, e_jmp );
	//	cpad = new CCallHook( (void *)0x00541E1C );
	//	cpad->enable( this, &SREvents::CPad__UpdateHook );

	//script->enable( this, &SREvents::Script );
	main->enable( this, &SREvents::Main );
#endif
}

SREvents::~SREvents() {
#if __has_include( <llmo/ccallhook.h>)
	delete cpad;
	delete script;
	delete main;
#endif
}

#if __has_include( <llmo/SRHook.hpp>)
bool SREvents::isKeyDown( int key ) {
	return keyDown[key];
}

bool SREvents::isCode( std::string code ) {
	std::transform( code.begin(), code.end(), code.begin(), ::toupper );
	if ( codeLen < code.length() ) return false;

	for ( size_t i = codeLen - code.length(); i < codeLen; ++i )
		if ( this->code[i] != (uint32_t)( code[i - ( codeLen - code.length() )] ) ) return false;

	memset( this->code, 0, sizeof( this->code ) );
	codeLen = 0;
	return true;
}

bool SREvents::isKeyComboDown( const std::vector<int> &combo ) {
	if ( combo.empty() ) return false;
	for ( auto key : combo )
		if ( !keyDown[key] ) return false;
	return true;
}
#endif

#if __has_include( <llmo/ccallhook.h>)
void SREvents::pressKey( int key, uint32_t time_ms ) {
	emuls[key] = { time_ms, false };
}
#endif

#if __has_include( <SRSignal/SRSignal.hpp>)
void SREvents::addCode( std::string code ) {
	std::transform( code.begin(), code.end(), code.begin(), ::toupper );
	if ( std::find( codes.begin(), codes.end(), code ) == codes.end() ) codes.push_back( code );
}

void SREvents::addCombo( const std::vector<int> &combo, const std::string &name ) {
	combos[name] = combo;
}

const std::vector<int> &SREvents::getCombo( const std::string &name ) {
	return combos[name];
}

void SREvents::deleteCode( std::string code ) {
	auto it = std::find( codes.begin(), codes.end(), code );
	if ( it != codes.end() ) codes.erase( it );
}

void SREvents::deleteCombo( const std::string &comboName ) {
	combos.erase( comboName );
}

void SREvents::hookEvent() {
	eventHook = true;
}
#endif

#if __has_include( <SRKeys/SRKeys.hpp>)
uint16_t SREvents::gameKeyState( SRKeys::GameKeys id ) {
#else

uint16_t SREvents::gameKeyState( uint8_t id ) {
#endif
	auto gameKeys = (uint16_t *)0xB73458;
#if __has_include( <SRKeys/SRKeys.hpp>)
	return gameKeys[id.number];
#else
	return gameKeys[id];
#endif
}

#if __has_include( <llmo/SRHook.hpp>)
#	if __has_include( <SRKeys/SRKeys.hpp>)
void SREvents::setGameKeyState( SRKeys::GameKeys id, uint16_t state ) {
	newKeyState[id.number].value = state;
	newKeyState[id.number].change = true;
#	else
void SREvents::setGameKeyState( uint8_t id, uint16_t state ) {
	newKeyState[id].value = state;
	newKeyState[id].change = true;
#	endif
}
#endif

#if __has_include( <llmo/SRHook.hpp>)
void SREvents::WndProc( SRHook::Info &info, size_t &retAddr, void *&hwnd, uint32_t &uMsg, uint32_t &wParam, uint32_t &lParam ) {
	// fix alt sticking
	if ( hwnd != *reinterpret_cast<void **>( 0xC97C1C ) && ( isKeyDown( VK_LMENU ) || isKeyDown( VK_RMENU ) ) ) {
		if ( GetAsyncKeyState( VK_LMENU ) & 0x01 )
			processKeyUp( VK_LMENU );
		else if ( GetAsyncKeyState( VK_RMENU ) & 0x01 )
			processKeyUp( VK_RMENU );
	} else if ( !isKeyDown( VK_LMENU ) && !isKeyDown( VK_RMENU ) ) {
		if ( GetAsyncKeyState( VK_LMENU ) & 0x01 )
			processKeyDown( VK_LMENU );
		else if ( GetAsyncKeyState( VK_RMENU ) & 0x01 )
			processKeyDown( VK_RMENU );
	}
	// process events
	if ( !Event( hwnd, uMsg, wParam, lParam ) ) {
		uMsg = 0;
		wParam = 0;
		lParam = 0;
	}
	// fix alt workaround in window mode
	if ( uMsg == 0x112 && ( wParam & 0xFFF0 ) == 0xF100 ) {
		info.skipOriginal = true;
		info.cpu.EAX = 0;
		info.cpu.ESP += 20;
		info.retAddr = retAddr;
	}
}

bool SREvents::Event( void *hwnd, uint32_t uMsg, uint32_t wParam, uint32_t lParam ) {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onEventProcFull( hwnd, uMsg, wParam, lParam );
#	endif
	if ( eventHook ) {
		eventHook = false;
		return false;
	}

	if ( hwnd != *reinterpret_cast<void **>( 0xC97C1C ) ) return true;

#	if __has_include( <SRSignal/SRSignal.hpp>)
	onEventProc( uMsg, wParam, lParam );
#	endif
	if ( eventHook ) {
		eventHook = false;
		return false;
	}

	if ( uMsg == WM_XBUTTONDOWN && ( wParam == 0x10020 || wParam == 0x20040 ) ) {
		if ( wParam == 0x10020 )
			wParam = VK_XBUTTON1;
		else
			wParam = VK_XBUTTON2;
		processKeyDown( wParam );
	} else if ( uMsg == WM_XBUTTONUP && ( wParam == 0x10000 || wParam == 0x20000 ) ) {
		if ( wParam == 0x10000 )
			wParam = VK_XBUTTON1;
		else
			wParam = VK_XBUTTON2;
		processKeyUp( wParam );
	} else if ( uMsg == WM_LBUTTONDOWN )
		processKeyDown( VK_LBUTTON );
	else if ( uMsg == WM_LBUTTONUP )
		processKeyUp( VK_LBUTTON );
	else if ( uMsg == WM_RBUTTONDOWN )
		processKeyDown( VK_RBUTTON );
	else if ( uMsg == WM_RBUTTONUP )
		processKeyUp( VK_RBUTTON );
	else if ( uMsg == WM_MBUTTONDOWN )
		processKeyDown( VK_MBUTTON );
	else if ( uMsg == WM_MBUTTONUP )
		processKeyUp( VK_MBUTTON );
	else if ( uMsg == WM_LBUTTONDBLCLK ) {
		processKeyDown( VK_LBUTTON );
		processKeyUp( VK_LBUTTON );
	} else if ( uMsg == WM_RBUTTONDBLCLK ) {
		processKeyDown( VK_RBUTTON );
		processKeyUp( VK_RBUTTON );
	} else if ( uMsg == WM_MBUTTONDBLCLK ) {
		processKeyDown( VK_MBUTTON );
		processKeyUp( VK_MBUTTON );
	} else if ( uMsg == WM_KEYDOWN || uMsg == WM_SYSKEYDOWN || uMsg == WM_KEYUP || uMsg == WM_SYSKEYUP ) {

		if ( wParam == VK_SHIFT || wParam == VK_CONTROL || wParam == VK_MENU ) {
			int scancode = ( lParam >> 16 ) & 0x00FF;
			if ( scancode == MapVirtualKey( VK_LSHIFT, 0 ) )
				wParam = VK_LSHIFT;
			else if ( scancode == MapVirtualKey( VK_RSHIFT, 0 ) )
				wParam = VK_RSHIFT;
			else if ( scancode == MapVirtualKey( VK_LCONTROL, 0 ) )
				wParam = VK_LCONTROL;
			else if ( scancode == MapVirtualKey( VK_RCONTROL, 0 ) )
				wParam = VK_RCONTROL;
			else if ( scancode == MapVirtualKey( VK_LMENU, 0 ) )
				wParam = VK_LMENU;
			else if ( scancode == MapVirtualKey( VK_RMENU, 0 ) )
				wParam = VK_RMENU;
		}
		if ( uMsg == WM_KEYDOWN || uMsg == WM_SYSKEYDOWN ) {
			processKeyDown( wParam );
			if ( codeLen == 120 ) {
				memcpy( code, (void *)( (size_t)code + sizeof( uint8_t ) ), 127 * sizeof( uint8_t ) );
				code[119] = wParam;
				code[120] = 0;
			} else {
				code[codeLen++] = wParam;
			}
#	if __has_include( <SRSignal/SRSignal.hpp>)
			processCodes();
#	endif
		} else
			processKeyUp( wParam );
	}
#	if __has_include( <SRSignal/SRSignal.hpp>)
	else if ( uMsg == WM_MOUSEWHEEL ) {
		if ( GET_WHEEL_DELTA_WPARAM( wParam ) < 0 )
			onScrollDown();
		else
			onScrollUp();
	}
#	endif

	if ( eventHook ) {
		eventHook = false;
		return false;
	}
	return true;
}
#endif

#if __has_include( <llmo/ccallhook.h>)
void SREvents::Script() {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onScriptLoop();
#	endif

	for ( auto it = emuls.begin(); it != emuls.end(); it++ ) {
		if ( !it->second.second ) {
			keybd_event( it->first, 0, 0, 0 );
			it->second.first += GetTickCount();
			it->second.second = true;
		} else if ( it->second.second && it->second.first < GetTickCount() ) {
			keybd_event( it->first, 0, 2, 0 );
			emuls.erase( it );
			it = emuls.begin();
			if ( emuls.empty() ) break;
		}
	}
}

void SREvents::Main() {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onMainLoop();
#	endif

	CPad_UpdateHook(); // wtf?
}
#endif

#if __has_include( <llmo/SRHook.hpp>)
void SREvents::processKeyDown( int key ) {
	if ( !keyDown[key] ) {
		keys++;
#	if __has_include( <SRSignal/SRSignal.hpp>)
		keyDown[key] = true;
		onKeyPressed( key );
		for ( auto &&combo : combos ) processCombo( combo.first );
#	endif
	}
	onKeyDown( key );
}

void SREvents::processKeyUp( int key ) {
	keyDown[key] = false;
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onKeyUp( key );
#	endif
	keys--;
}

void SREvents::processCodes() {
	for ( auto &&buf : codes ) {

		if ( codeLen < buf.length() ) continue;

		auto str = (char *)( (size_t)code + ( codeLen - buf.length() ) );
#	if __has_include( <SRSignal/SRSignal.hpp>)
		if ( buf == str ) onCode( buf );
#	endif
	}
}

void SREvents::processCombo( const std::string &comboName ) {
	auto combo = combos[comboName];
	for ( int comboKey : combo )
		if ( !keyDown[comboKey] ) return;
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCombo( comboName );
#	endif
}

void SREvents::CPad_UpdateHook() {
	auto gameKeys = (uint16_t *)0xB73458;
	auto oldGameKeys = (uint16_t *)0xB73488;
	for ( uint8_t i = 0; i < 24; ++i ) {
#	if __has_include( <SRSignal/SRSignal.hpp>)
		onGameKeyState( i, gameKeys[i] );
#	endif
		if ( oldGameKeys[i] != gameKeys[i] ) onGameKeyStateChanged( i, gameKeys[i] );
		if ( newKeyState[i].change ) {
			gameKeys[i] = newKeyState[i].value;
			oldGameKeys[i] = newKeyState[i].value;
			newKeyState[i].change = false;
		}
	}
}
#endif
