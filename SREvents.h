#pragma once

#include <map>

#if __has_include( <SRKeys/SRKeys.hpp>)
#	include <SRKeys/SRKeys.hpp>
#else
#	include <vector>
#	include <string>
#endif
#if __has_include( <SRSignal/SRSignal.hpp>)
#	include <SRSignal/SRSignal.hpp>
#endif
#if __has_include( <llmo/SRHook.hpp>)
#	include <llmo/SRHook.hpp>
#endif

class SREvents {
public:
	SREvents();
	~SREvents();

#if __has_include( <llmo/SRHook.hpp>)
	/**
	 * \brief Проверяет зажата ли клавишами
	 * \param[in] key Код клавиши
	 * \return \b bool -> состояние клавиши (зажата/отпущена)
	 */
	[[nodiscard]] bool isKeyDown( int key );
	/**
	 * \brief Проверяет был ли введен чит-код
	 * \param[in] code код
	 * \return \b bool
	 */
	[[nodiscard]] bool isCode( std::string code );
	/**
	 * \brief Проверяет зажата ли комбинация клавиш
	 * \param[in] combo комбинация клавиш
	 * \return \b bool
	 */
	[[nodiscard]] bool isKeyComboDown( const std::vector<int> &combo );
#endif

#if __has_include( <llmo/ccallhook.h>)
	/**
	 * \brief Эмуляция нажатия клавиши
	 * \param[in] key Код клавиши
	 * \param[in] time_ms Как долго клавиша будет зажата (в милисекундах)
	 */
	void pressKey( int key, uint32_t time_ms = 20 );
#endif


#if __has_include( <SRSignal/SRSignal.hpp>)
	/**
	 * \brief Добавляет чит-код в обработку
	 * \detail При вводе этого кода будет вызван сигнал eSRKeys::SRKeys_eventCode
	 * \param[in] code Чит-код
	 */
	void addCode( std::string code );
	/**
	 * \brief Добавляет комбинацию клавишь в обработку
	 * \detail При зажатии данной комбинации клавиш будет вызван сигнал eSRKeys::SRKeys_eventCombo
	 * \param combo Комбинация клавиш
	 * \param name Название комбинации
	 */
	void addCombo( const std::vector<int> &combo, const std::string &name );

	/**
	 * \brief Получить комбинацию клавишь по ее названию
	 * \param name Название комбинации клавиш
	 * \return \b KeyCombo -> комбинация клавиш
	 */
	[[nodiscard]] const std::vector<int> &getCombo( const std::string &name );

	/**
	 * \brief Удаление чит-код из обработки
	 * \param code Чит-код
	 */
	void deleteCode( std::string code );
	/**
	 * \brief Удаление комбинации клавиш из обработки
	 * \param comboName Название комбинации клавиш
	 */
	void deleteCombo( const std::string &comboName );

	/**
	 * @brief Прерывает обработку текущего события игрой и другими плагинами
	 * @details Предполагается, что слот вызвавший данный метод, сам вызвал игровой обработчик событий, если
	 * данное событие не надо было "заглушить".
	 */
	void hookEvent();
#endif

	/**
	 * @brief Получает состояние игровой клавиши
	 * @details В игре всего 48 клавиш. Известны первые 19 - SRKeys::GameKeys
	 * @details Один и тот же номер клавиши может иметь различные коды состояний, которые определяют значение
	 * данной клавиши
	 * @param id Номер клавиши
	 * @return Состояние клавиши
	 */
#if __has_include( <SRKeys/SRKeys.hpp>)
	[[nodiscard]] static uint16_t gameKeyState( SRKeys::GameKeys id );
#else
	[[nodiscard]] static uint16_t gameKeyState( uint8_t id );
#endif

#if __has_include( <llmo/SRHook.hpp>)
	/**
	 * @brief Устанавливает новое состояние игровой клавиши
	 * @details В игре всего 48 клавиш. Известны первые 19 - SRKeys::GameKeys
	 * @details Один и тот же номер клавиши может иметь различные коды состояний, которые определяют значение
	 * данной клавиши
	 * @param id Номер клавиши
	 * @param state Состояние
	 */
#	if __has_include( <SRKeys/SRKeys.hpp>)
	void setGameKeyState( SRKeys::GameKeys id, uint16_t state );
#	else
	void setGameKeyState( uint8_t id, uint16_t state );
#	endif
#endif

#if __has_include( <SRSignal/SRSignal.hpp>)
#	if __has_include( <llmo/SRHook.hpp>)
	SRSignal<uint32_t, uint32_t, uint32_t> onEventProc;
	SRSignal<void *, uint32_t, uint32_t, uint32_t> onEventProcFull;
#	endif
	SRSignal<> onScriptLoop;
	SRSignal<> onMainLoop;

#	if __has_include( <llmo/SRHook.hpp>)
	SRSignal<int> onKeyDown;
	SRSignal<int> onKeyUp;
	SRSignal<int> onKeyPressed;
	SRSignal<std::string> onCode;
	SRSignal<std::string> onCombo;
	SRSignal<> onScrollUp;
	SRSignal<> onScrollDown;
#	endif
#	if __has_include( <SRKeys/SRKeys.hpp>)
	SRSignal<SRKeys::GameKeys, uint16_t> onGameKeyState;
	SRSignal<SRKeys::GameKeys, uint16_t> onGameKeyStateChanged;
#	else
	SRSignal<uint8_t, uint16_t> onGameKeyState;
	SRSignal<uint8_t, uint16_t> onGameKeyStateChanged;
#	endif
#endif

protected:
	bool eventHook;

#if __has_include( <llmo/SRHook.hpp>)
	void WndProc( SRHook::Info &info, size_t &retAddr, void *&hwnd, uint32_t &uMsg, uint32_t &wParam, uint32_t &lParam );
	bool Event( void *hwnd, uint32_t uMsg, uint32_t wParam, uint32_t lParam );
#endif
#if __has_include( <llmo/ccallhook.h>)
	void Script();
	void Main();
#endif

#if __has_include( <llmo/SRHook.hpp>)
	void processKeyDown( int key );
	void processKeyUp( int key );
	void processCodes();
	void processCombo( const std::string &comboName );

	void CPad_UpdateHook();
#endif

private:
	struct changeKeyState_t {
		uint16_t value = 0;
		bool change = false;
	};

#if __has_include( <llmo/ccallhook.h>)
	class CCallHook *main;
	class CCallHook *script;
	class CCallHook *cpad;
#endif
#if __has_include( <llmo/SRHook.hpp>)
	SRHook::Hook<size_t, void *, uint32_t, uint32_t, uint32_t> events{ 0x747EB0, 5 };
#endif

	std::map<int, bool> keyDown;
	uint8_t code[128]{ 0 };
	uint32_t codeLen = 0;
	uint32_t keys = 0;
	std::map<std::string, std::vector<int>> combos;
	std::vector<std::string> codes;
	std::map<int, std::pair<uint32_t, bool>> emuls;
	changeKeyState_t newKeyState[24]{ 0 };
};
